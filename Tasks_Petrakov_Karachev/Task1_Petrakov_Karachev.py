from flask import Flask, request, jsonify
import redis
import random

app = Flask(__name__)
redis_db = redis.StrictRedis(host='redis', port=6379, decode_responses=True)


@app.route('/search', methods=['POST'])
def search():
    target = int(request.json['target'])
    array = get_or_generate_array()
    index = binary_search(array=array, target=target)
    return jsonify({'index': index})


def get_or_generate_array():
    if not redis_db.exists('array'):
        size = 100
        array = generate_sorted_array(size)
        redis_db.set('array', ','.join(map(str, array)))
    else:
        array = list(map(int, redis_db.get('array').split(',')))
    return array


def generate_sorted_array(size):
    return sorted(random.choices(range(100), k=size))


def binary_search(array, target):
    left, right = 0, len(array) - 1
    while left <= right:
        mid = (left + right) // 2
        if array[mid] == target:
            return mid
        elif array[mid] < target:
            left = mid + 1
        else:
            right = mid - 1
    return -1


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080)
