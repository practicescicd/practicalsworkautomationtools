from Task1_Petrakov_Karachev import binary_search, generate_sorted_array


def test_binary_search():
    array = [1, 2, 3, 4, 5]
    assert binary_search(array, 3) == 2
    assert binary_search(array, 6) == -1


def test_generate_sorted_array():
    size = 10
    array = generate_sorted_array(size)
    assert len(array) == size
    assert array == sorted(array)
