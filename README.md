# PracticalsWorkAutomationTools



## Задание №2
### Часть 1

Написать программу на **любом языке** программирования - **выбран python**, которое выполняет следующую задачу поиска требуемого значения в массиве случайных чисел. Требования к программе:

- Массив генерируется случайно при запуске приложения.
- Массив должен быть отсортирован по порядку.
- Выполнять поиск с помощью бинарного алгоритма.
- Размер массива - 100 чисел.
- Пользователь при запуске программы вводит искомое значение.
- Значение передаётся через параметр командной строки.

**Запуск: python3 Task1_Petrakov_Karachev.py 20/22 (12.11.2003 = 10 * 2) или (21.12.2003 = 11 * 2 = 22)**

![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/python1.png)

### Часть 2

С помощью dockerfile создать образ приложения.

Результатами выполнения задания будут: dockren run app **(индекс)**

 - Листинг dockerfile
 - Скриншот выполнения программы при помощи docker run

 ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/Dockerfile1.png)
 

 - sh скрипт как точка входа

 ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/Sh1.png)

 - Итоговый результат:

 ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/Build.png)

 - docker run --rm app 22:
    - **(rm для автоматического удаления контейнера после создания и выполнения)**

 ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/Run.png)

 - Обработка ошибок

 ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/Error1.png)

 ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task2/Error2.png)


## Задание №3
### Часть 1

Найти способ воспользоваться сервисом GitLab. Разрешается локально развернуть сервис для себя или группы с помощью удалённого хостинга. Можно попробовать зарегистрироваться через Google аккаунт.
При успешном получении доступа к сервису, необходимо создать репозиторий для проекта из задания 2 и загрузить всю исходную базу.

1. Установка образа и создание контейнера по образу

   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/GitlabRunnerVolume.png)
   
2. Запуск контейнера и выбор executor

   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/GitlabRunnerExec.png)
   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/GitlabRunnerExecutor.png)

### Часть 2

Далее, согласно [гайду](https://docs.gitlab.com/ee/ci/), настроить собственный gitlab-runner для проекта и написать сценарий .gitlab-ci.yml, содержащий две стадии:

1. Сценарий

   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/ListingYml.png)

2. Pipeline

   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/Pipeline.png)

3. PEP8 И TESTS

   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/Flake8.png)
   ![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task3/Test.png)

## Задание #4

Модернизировать приложение таким образом, чтобы оно было частью бэкенд сервиса – для этого можно использовать любой фреймворк (FastAPI, ASP.NET, Django, Spring Boot, etc…). 

Требования следующие:

- Пользователь отправляет один запрос на энд-поинт сервиса содержащий искомое число.
- Сервер возвращает либо индекс в массиве, где это число расположено, либо -1, если его нет.
- При **первом запуске сервера** массив генерируется с нуля.
- Значения массива сохраняются в базу данных redis и загружаются оттуда при последующих перезапусках сервера.
- Приложение принимает запросы на порт 8080.

![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task4/gitlab-ci.png)

![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task4/docker-compose.png)

![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task4/docker.png)

![Alt text](Tasks_Petrakov_Karachev/ScreenShots/Task4/postman.png)